/*
 * FeedSearcher.java (StAX)
  * @author Arita  Manoman
 * id : 533040744-7
 */
package coe.kku.webservice;

import javax.servlet.ServletException;
import javax.servlet.http.*;
import java.net.*;
import javax.xml.stream.*;
import javax.xml.stream.events.*;
import java.io.*;
import javax.servlet.annotation.WebServlet;

@WebServlet(name = "FeedSearcher2", urlPatterns = {"/FeedSearcher2"})
public class FeedSearcher2 extends HttpServlet {

    XMLEventReader reader;

    protected void processRequest(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        response.setContentType("text/html;charset=UTF-8");
        request.setCharacterEncoding("UTF-8");
        PrintWriter out = response.getWriter();
        String inURL = request.getParameter("url");
        String inKeyword = request.getParameter("keyword");
        boolean titleFound = false;
        boolean linkFound = false;
        boolean itemFound = false;
        boolean keyFound = false;
        String inLink = null;
        String inTitle = null;
        String eName;
        try {
            URL u = new URL(inURL);
            InputStream in = u.openStream();
            XMLInputFactory factory = XMLInputFactory.newInstance();
            reader = factory.createXMLEventReader(in);
            out.print(
            "<html><body><table	border	='1'><tr> <th>Title</th> <th>Link</th > </tr>");
while (reader.hasNext()) {
                XMLEvent event = reader.nextEvent();
                if (event.isStartElement()) {
                    StartElement element = (StartElement) event;
                    eName = element.getName().getLocalPart();
                    if (eName.equals("item")) {
                        itemFound = true;
                    }
                    if (itemFound && eName.equals("link")) {
                        linkFound = true;
                        out.print("<td>");
                    }
                    if (itemFound && eName.equals("title")) {
                        titleFound = true;
                        out.print("<tr><td>");
                    }
                }
                if (event.isEndElement()) {
                    EndElement element = (EndElement) event;
                    eName = element.getName().getLocalPart();
                    if (eName.equals("item")) {
                        itemFound = false;
                    }
                    if (itemFound && eName.equals("link")) {
                        out.print("</td></tr>");
                        linkFound = false;
                    }
                    if (itemFound && eName.equals("title")) {
                        out.print("</td>");
                        titleFound = false;
                    }
                }
                if (event.isCharacters()) {
                    Characters characters = (Characters) event;
                    if (linkFound) {
                        inLink=characters.getData();
                    }
                    if (titleFound) {
                        inTitle=characters.getData();
                         if ((inTitle.toLowerCase()).contains(inKeyword.toLowerCase())) {
                                out.print("<tr><td>");
                                out.print(inTitle);
                                out.print("<td>");
                                out.print(inLink);
                                out.print("</td>");
                                out.print("</td>");
                         }
                    }
                }
            }			
            reader.close();
            out.print("</table></body></html>");
        } catch (Exception ex) {
            ex.printStackTrace(System.err);
        } finally {
            out.close();
        }
    }

    @Override
    protected void doGet(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        processRequest(request, response);
    }

    @Override
    protected void doPost(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        processRequest(request, response);
    }

    @Override
    public String getServletInfo() {
        return "Short	description";
    }
}