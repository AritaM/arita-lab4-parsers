/*
 * FeedWriterForm.java (StAX)
 * @author Arita  Manoman
 * id : 533040744-7
 */
package kku.coe.webservice;

import java.io.*;
import javax.servlet.*;
import javax.servlet.annotation.*;
import javax.servlet.http.*;
import javax.xml.stream.*;
import javax.xml.stream.events.*;

@WebServlet(name = "FeedWriterForm2", urlPatterns = {"/Arita-FeedWriterForm2"})
public class FeedWriterForm2 extends HttpServlet {

    String filename = "C:/Users/home/Documents/NetBeansProjects/Arita-FeedWriterForm2/web/feed.xml";
    File file = new File(filename);

    protected void processRequest(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        response.setContentType("text/html;charset=UTF-8");
        request.setCharacterEncoding("UTF-8");
        PrintWriter out = response.getWriter();

        try {
            String rtitle = request.getParameter("title");
            String rurl = request.getParameter("url");
            String rdescription = request.getParameter("description");

            if (file.exists()) {
                new FeedWriterForm2().updateRssFeed(rtitle, rdescription, rurl);
            } else {
                new FeedWriterForm2().createRssFeed(rtitle, rdescription, rurl);
            }

            out.println("<b> <a href='http://localhost:8080/Arita-FeedWriterForm2/feed.xml'>" + "Rss Feed</a> was create successfully</b>");

        } catch (Exception e) {
            System.out.println();
        }
    }

    private void createRssFeed(String rssTitle, String rssDescription, String rssUrl) throws Exception {
        XMLOutputFactory xof = XMLOutputFactory.newInstance();
        XMLStreamWriter xtw = xof.createXMLStreamWriter(new OutputStreamWriter(new FileOutputStream(file)));
        xtw.writeStartDocument();
        
        xtw.writeStartElement("rss");
        xtw.writeAttribute("version", "2.0");
        
        xtw.writeStartElement("channel");
        xtw.writeStartElement("title");
        xtw.writeCharacters("Khon Kean University RSS Feed");
        xtw.writeEndElement();
        xtw.writeStartElement("description");
        xtw.writeCharacters("Khon Kean University Information News RSS Feed");
        xtw.writeEndElement();
        xtw.writeStartElement("link");
        xtw.writeCharacters("http://www.kku.ac.th");
        xtw.writeEndElement();
        xtw.writeStartElement("lang");
        xtw.writeCharacters("en~th");
        xtw.writeEndElement();

        xtw.writeStartElement("item");
        xtw.writeStartElement("title");
        xtw.writeCharacters(rssTitle);
        xtw.writeEndElement();
        xtw.writeStartElement("description");
        xtw.writeCharacters(rssDescription);
        xtw.writeEndElement();
        xtw.writeStartElement("link");
        xtw.writeCharacters(rssUrl);
        xtw.writeEndElement();
        xtw.writeStartElement("pubDate");
        xtw.writeCharacters((new java.util.Date()).toString());
        xtw.writeEndElement();
        xtw.writeEndElement();
        xtw.writeEndElement();
        xtw.writeEndElement();
        xtw.writeEndDocument();
        xtw.flush();
        xtw.close();
    }

    private void updateRssFeed(String rssTitle, String rssDescription, String rssUrl) throws Exception {
        XMLInputFactory xif = XMLInputFactory.newInstance();
        XMLEventReader feedReader = xif.createXMLEventReader(new InputStreamReader(new FileInputStream(file)));
        XMLOutputFactory xof = XMLOutputFactory.newInstance();
        XMLStreamWriter feedWriter = xof.createXMLStreamWriter(new OutputStreamWriter(new FileOutputStream(file)));
        String elementName;

        while (feedReader.hasNext()) {
            XMLEvent event = feedReader.nextEvent();

            if (event.getEventType() == XMLEvent.START_DOCUMENT) {
                feedWriter.writeStartDocument();
            }

            if (event.getEventType() == XMLEvent.START_ELEMENT) {
                StartElement element = (StartElement) event;
                elementName = element.getName().getLocalPart();
                if (elementName.equals("rss")) {
                    feedWriter.writeStartElement("rss");
                    feedWriter.writeAttribute("version", "2.0");
                } else {
                    feedWriter.writeStartElement(elementName);
                }
            }

            if (event.isEndElement()) {
                EndElement element = (EndElement) event;
                elementName = element.getName().getLocalPart();
                if (elementName.equals("channel")) {
                    feedWriter.writeStartElement("item");
                    feedWriter.writeStartElement("title");
                    feedWriter.writeCharacters(rssTitle);
                    feedWriter.writeEndElement();
                    feedWriter.writeStartElement("description");
                    feedWriter.writeCharacters(rssDescription);
                    feedWriter.writeEndElement();
                    feedWriter.writeStartElement("link");
                    feedWriter.writeCharacters(rssUrl);
                    feedWriter.writeEndElement();
                    feedWriter.writeStartElement("pubDate");
                    feedWriter.writeCharacters((new java.util.Date()).toString());
                    feedWriter.writeEndElement();
                    feedWriter.writeEndElement();
                    feedWriter.writeEndDocument();
                    feedWriter.flush();
                    feedWriter.close();
                    return;
                } else {
                    feedWriter.writeEndElement();
                }
            }
            
            if (event.isCharacters()) {
                Characters characters = (Characters) event;
                feedWriter.writeCharacters(characters.getData().toString());
            }
        }
        feedWriter.flush();
        feedWriter.close();
    }

    @Override
    protected void doGet(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        processRequest(request, response);
    }

    @Override
    protected void doPost(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        processRequest(request, response);
    }

    @Override
    public String getServletInfo() {
        return "Short description";
    }
}