/*
 * XMLFileWriter.java (DOM)
 * @author Arita  Manoman
 * id : 533040744-7
 */
package kku.coe.webservice;

import java.io.File;
import java.io.IOException;
import java.io.PrintWriter;
import java.io.StringWriter;
import java.util.logging.Level;
import java.util.logging.Logger;
import javassist.convert.Transformer;
import javax.xml.parsers.DocumentBuilder;
import javax.xml.parsers.DocumentBuilderFactory;
import javax.xml.parsers.ParserConfigurationException;
import javax.xml.transform.OutputKeys;
import javax.xml.transform.TransformerFactory;
import javax.xml.transform.dom.DOMSource;
import javax.xml.transform.stream.StreamResult;
import org.w3c.dom.Element;
import org.w3c.dom.Text;
import org.w3c.dom.*;

public class XMLFileWriter {

    public static void main(String argv[]) {
        try {
            DocumentBuilderFactory builderFactory = DocumentBuilderFactory.newInstance();
            DocumentBuilder docBuilder = builderFactory.newDocumentBuilder();
            Document doc = docBuilder.newDocument();

            Element quotes = doc.createElement("quotes");
            doc.appendChild(quotes);

            Element quote1 = doc.createElement("quote");
            quotes.appendChild(quote1);
            
            Element words = doc.createElement("words");
            quote1.appendChild(words);
            Text wt1 = doc.createTextNode("Time is more value than money. You can get more money, but you cannot get more time");
            words.appendChild(wt1);
            Element by = doc.createElement("by");
            quote1.appendChild(by);
            Text b1 = doc.createTextNode("Jim Rohn");
            by.appendChild(b1);

            Element quote2 = doc.createElement("quote");
            quotes.appendChild(quote2);
            Element words2 = doc.createElement("words");
            quote2.appendChild(words2);
            Text wt2 = doc.createTextNode("เมื่อทำอะไรสำเร็จ แม้เป็นก้าวเล็กๆ ของตัวเอง ก็ควรรู้จักให้รางวัลตัวเองบ้าง");
            words2.appendChild(wt2);
            Element by2 = doc.createElement("by");
            quote2.appendChild(by2);
            Text b2 = doc.createTextNode("ว. วชิรเมธี");
            by2.appendChild(b2);
                     
            TransformerFactory factory = TransformerFactory.newInstance();
            javax.xml.transform.Transformer transformer = factory.newTransformer();
            transformer.setOutputProperty(OutputKeys.INDENT, "yes");
            StringWriter sw = new StringWriter();
            StreamResult result = new StreamResult(new File("C:\\Users\\home\\Documents\\NetBeansProjects\\Arita-XMLFileWriter\\quotes.xml"));
            DOMSource source = new DOMSource(doc);
            transformer.transform(source, result);
            String xmlString = sw.toString();
            String filename = "quotes.xml";
            transformer.transform(source, result);

        } catch (Exception e) {
            System.out.println(e);
        }
    }
}
