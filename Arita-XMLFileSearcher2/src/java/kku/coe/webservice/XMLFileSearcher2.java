/*
 * XMLFileSearcher.java (StAX)
 * @author Arita  Manoman
 * id : 533040744-7
 */
package kku.coe.webservice;

import javax.xml.parsers.DocumentBuilderFactory;
import javax.xml.parsers.DocumentBuilder;
import org.w3c.dom.Document;
import org.w3c.dom.NodeList;
import org.w3c.dom.Node;
import org.w3c.dom.Element;
import java.io.File;
import java.io.FileInputStream;
import java.io.FilterInputStream;
import java.io.InputStream;
import java.util.Scanner;
import javax.xml.stream.XMLEventReader;
import javax.xml.stream.XMLInputFactory;
import javax.xml.stream.events.Characters;
import javax.xml.stream.events.EndElement;
import javax.xml.stream.events.StartElement;
import javax.xml.stream.events.XMLEvent;
import org.w3c.dom.CharacterData;

public class XMLFileSearcher2 {

    public static void main(String argv[]) {
        String eName;
        File f = new File("C:/Users/home/Documents/NetBeansProjects/Arita-XMLFileSearcher/keyword.txt");
        boolean quoteFound = false;
        boolean wordsFound = false;
        boolean byFound = false;

        String byXml = null;
        String wordsXml = null;

        try {
            Scanner sc = new Scanner(new FileInputStream(f));
            String s;
            while (sc.hasNextLine()) {
                s = sc.nextLine();
                //System.out.println(s);

                XMLFileSearcher2 read = new XMLFileSearcher2();
                XMLInputFactory inputFactory = XMLInputFactory.newInstance();

                InputStream in = new FileInputStream("C:\\Users\\home\\Documents\\NetBeansProjects\\quotes.xml");
                XMLEventReader reader = inputFactory.createXMLEventReader(in);
                //System.out.println(in);

                while (reader.hasNext()) {
                    XMLEvent event = reader.nextEvent();
                    if (event.isStartElement()) {
                        StartElement element = (StartElement) event;
                        eName = element.getName().getLocalPart();
                        if (eName.equals("quote")) {
                            quoteFound = true;
                        }
                        if (quoteFound && eName.equals("words")) {
                            wordsFound = true;
                        }
                        if (quoteFound && eName.equals("by")) {
                            byFound = true;
                        }
                    }
                    if (event.isEndElement()) {
                        EndElement element = (EndElement) event;
                        eName = element.getName().getLocalPart();
                        if (eName.equals("quote")) {
                            quoteFound = false;
                        }
                        if (wordsFound && eName.equals("words")) {
                            wordsFound = false;
                        }
                        if (byFound && eName.equals("by")) {
                            byFound = false;
                        }
                    }
                    if (event.isCharacters()) {
                        Characters characters = (Characters) event;
                        //System.out.println(characters.getData());
                        if (wordsFound) {
                            wordsXml = characters.getData();
                            //System.out.println(wordsXml);
                        }
                        if (byFound) {
                            byXml = characters.getData();
                            //System.out.println(byXml);
                            if (byXml.toLowerCase().contains(s.toLowerCase())) {
                                System.out.println(wordsXml + " by " + byXml);
                            }
                        }
                    }
                }
            }

        } catch (Exception ex) {
        }
    }
}
