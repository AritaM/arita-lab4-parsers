/*
 * XMLFileSearcher.java (DOM)
  * @author Arita  Manoman
 * id : 533040744-7
 */
package kku.coe.webservice;

import javax.xml.parsers.DocumentBuilderFactory;
import javax.xml.parsers.DocumentBuilder;
import org.w3c.dom.Document;
import org.w3c.dom.NodeList;
import org.w3c.dom.Node;
import org.w3c.dom.Element;
import java.io.File;
import java.io.FileInputStream;
import java.io.FilterInputStream;
import java.util.Scanner;
import org.w3c.dom.CharacterData;

public class XMLFileSearcher {

    public static void main(String argv[]) {
        File f = new File("C:/Users/home/Documents/NetBeansProjects/Arita-XMLFileSearcher/keyword.txt");
        try {
            Scanner sc = new Scanner(new FileInputStream(f));
            String s;
            while (sc.hasNextLine()) {
                s = sc.nextLine();
                //System.out.println(s);
                try {
                    File fXmlFile = new File("C:\\Users\\home\\Documents\\NetBeansProjects\\quotes.xml");
                    DocumentBuilderFactory dbFactory = DocumentBuilderFactory.newInstance();
                    DocumentBuilder dBuilder = dbFactory.newDocumentBuilder();
                    Document doc = dBuilder.parse(fXmlFile);
                    doc.getDocumentElement().normalize();

                    NodeList nList = doc.getElementsByTagName("quote");
                    
                    for (int temp = 0; temp < nList.getLength(); temp++) {

                        Element eElement = (Element) nList.item(temp);
                        if (getElemVal(eElement, "by").toLowerCase().contains(s.toLowerCase())) {
                            System.out.println(getElemVal(eElement, "words") + " by " + getElemVal(eElement, "by"));
                            //System.out.println(getElemVal(eElement,"by"));
                        }
                    }
                } catch (Exception e) {
                    e.printStackTrace();
                }
            }
        } catch (Exception ex) {
            ex.printStackTrace();
        }
    }

    private static String getTagValue(String sTag, Element eElement) {
        NodeList nlList = eElement.getElementsByTagName(sTag).item(0).getChildNodes();

        Node nValue = (Node) nlList.item(0);

        return nValue.getNodeValue();
    }

    private static String getElemVal(Element parent, String lable) {
        Element e = (Element) parent.getElementsByTagName(lable).item(0);
        try {
            Node child = e.getFirstChild();
            if (child instanceof CharacterData) {
                CharacterData cd = (CharacterData) child;
                return cd.getData();
            }
        } catch (Exception ex) {
        }
        return "";
    }
}